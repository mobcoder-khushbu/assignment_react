import React from 'react';
import Signup from './components/Signup';
import Login from './components/Login';
import Reset from './components/resetPassword';
import Forgot from './components/forgot';
import BackLogin from './components/backtologin';
import Dashboard from './components/dashboard';
import ListUser from  './components/ListUsers';
import Navbar from  './components/navbar';
import Change from  './components/changePassword';
import Profile from  './components/profile';
import './App.css';
import {Route,Switch, BrowserRouter as Router} from 'react-router-dom';
function App() {
  return (
    <div className="App">
      <Router>
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/signup' component={Signup} />
        <Route path='/forgot-password' component={Forgot} />
        <Route path='/resetPassword' component={Reset} />
        <Route path='/backtoLogin' component={BackLogin} />
        <Route path='/dashboard' component={Dashboard} />
        <Route path='/userList' component={ListUser} />
        <Route path='/logout' component={Navbar} />
        <Route path='/changePassword' component={Change} />
        <Route path='/profile' component={Profile} />
        </Switch>
      </Router>
    </div>
  );
}
const Home = () => {
  return(
    <div>
      <Login />
    </div>
  )
}
export default App;
