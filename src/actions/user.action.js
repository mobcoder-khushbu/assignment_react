import * as actionType from './actionType';
import { signup,login,forgot,reset,getData,logout } from '../services';



/*Signup Action perform here */

export const signupAction = params =>  dispatch => {
    dispatch({ type: actionType.GET_AUTH_REQUEST });
    let user = signup(params);
        console.log('user',user);
        dispatch({ type: actionType.GET_AUTH_SUCCESS, payload:user });
    }

 /*Login Action perform here */

export const loginAction = params => dispatch => {
    dispatch({ type: actionType.GET_AUTH_REQUEST });
    let user = login(params);
    console.log('user',user);
    
        dispatch({ type: actionType.GET_AUTH_SUCCESS, payload:user });
}

export const logoutAction=() =>dispatch=>{
    dispatch({type:actionType.LOGOUT})
    let user =logout();
    dispatch ({ type: actionType.LOGOUT, payload:user });
}

/*Forgot Password Action perform here */
    
export const forgotAction = params => dispatch => {
    dispatch({ type: actionType.GET_FORGOT_REQUEST });
    let user = forgot(params);
    console.log('user',user);
        dispatch({ type: actionType.GET_FORGOT_SUCCESS, payload:user });
        
}

/*Reset Password Action perform here */

export const resetAction = (params,token) => dispatch => {
    dispatch({ type: actionType.GET_RESET_REQUEST });
    let user = reset({params,token});
    let url_string = window.location.href;
    let urlHead = new URL(url_string);
    user.token= urlHead.searchParams.get("token");
    console.log('user',user);
        dispatch({ type: actionType.GET_RESET_SUCCESS, payload:user });
        
}

/*Fetching Data Action perform here */

export const  getUser=()=>{
    return (dispatch)=>{
            getData().then(res=>{
                dispatch( {
                    type:actionType.GET_DATA,
                    payload:res.data.responseData
                })
            })
        
     
    }
}