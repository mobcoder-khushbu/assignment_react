import React from 'react'
import { Chart } from 'react-charts'
 
function MyChart() {
  const data = React.useMemo(
    () => [
      {
        label: 'Series 1',
        data: [{ x: 1, y: 100 }, { x: 2, y: 500 }, { x: 3, y: 1000 },
        { x: 4, y: 2000 },{ x: 5, y: 3000 },{ x: 6, y: 0 },
        { x: 7, y: 0 },{ x: 8, y: 0 },{ x: 9, y: 0 },{ x: 10, y: 0 }]
      },
    ],
    []
  )
 
  const axes = React.useMemo(
    () => [
      { primary: true, type: 'linear', position: 'bottom' },
      { type: 'linear', position: 'right' }
    ],
    []
  )
 
  return (
    <div
      style={{
        width: '100%',
        height: '300px'
      }}
    >
      <Chart data={data} axes={axes} />
    </div>
  )
}
export default MyChart;