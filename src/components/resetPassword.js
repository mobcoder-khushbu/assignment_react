import React, { useReducer } from 'react';
import './signup.css';

import { resetAction } from '../actions';
import {connect} from 'react-redux';
import {useHistory} from 'react-router-dom';

const Reset = props => {
     let history = useHistory();

    const [userInput, setUserInput] = useReducer((state, newState) => ({...state, ...newState}),
        {
            password: '',
            cnfpassword:''
        }
    );

    const [error, setError] = useReducer((state, newState) => ({...state, ...newState}),
        {
            password: ''
            
        }
    );



    const handleChange = e => {
        setUserInput({[e.target.name]: e.target.value});
        setError({[e.target.name]: ''});
    }



    const handleValidate = () => {
        let validate = true;
        let {password, cnfpassword} = userInput;
        if(password === cnfpassword && password===''){
            setError({password: 'password is require'});
            validate = false
        }
        else if(password !== cnfpassword){
            setError({password:'password is not match'})
            validate=false
        }

        return validate;
    }


    const handleSubmit = e => {
        e.preventDefault();

        if(handleValidate()){
            let { password,cnfpassword} = userInput;
            let params = {
                password,
                cnfpassword
            }

            props.resetAction(params)
            console.log('params',params);
            history.push('/');
        }
    }
return(
        <div> 
        <div className="container-fluid">
        <img src={require('./Images/background.jpg')} className="background" alt="food"/>
        <div className="carousel-caption">
        <ul className="nav">
        <li className="nav-item">
        <img src={require('./Images/Group 81.png')} alt="icon" className="logo"/>
        <span className="logo-text">GURU</span>
        </li>
        </ul>
        
        
        <div className="row form">
        <div className="col-sm-2"></div>
        <div className="col-sm-6">
        <form onSubmit={handleSubmit}>
        <img src={require('./Images/Group 81.png')} alt="icon"/><br/><br/>
        <span className="login-form-title">Reset Password</span>
        <div className="Form">

        <div className="form-group">
		<input className="form-control input-form" type="password" name="password" placeholder="Password" 
        value={userInput.password} onChange={handleChange} />
        <div className="error">{error.password}</div>
        </div>
        <div className="form-group">
		<input className="form-control input-form" type="password" name="cnfpassword" placeholder="Confirm Password" 
        value={userInput.cnfpassword} onChange={handleChange} />
        <div className="error">{error.password}</div>
        </div>
       
        
        <div>
		<button className="btn btn-primary btn" type="submit" to='./'>
			Reset Password
		</button>
		</div>
        <div><br/>
        
        </div>
        </div>
        <div className="col-sm-3"></div>
        {props.errorMsg ? <h3>{props.errorMsg}</h3> : ''}
        </form>
        </div>
        </div>
        
         </div>
        </div>
         </div>
         
         
        );
    }

const mapStateToProps = state => {
    console.log(state, 'state')
    let { errorMsg } = state.auth; 
    return {
        errorMsg
    };
}
  
const mapDispatchToProps = dispatch => ({
    resetAction: params => dispatch(resetAction(params)),
});
  
export default connect(mapStateToProps, mapDispatchToProps)(Reset);