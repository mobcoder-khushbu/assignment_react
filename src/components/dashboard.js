import React from 'react';
import Header from './Header';
import Navbar from './navbar';
import './header.css';
import Chart from './chart';
const Dashboard = () => {

    return(
        <div className="container-fluid">
        <div className="row">
        <div className="nav">
            <Header/>
        </div>
        
         <div className="right-nav">
         <Navbar/>
         <div className="row">
            <div className="card card-style">
            <img src={require('./Images/doller.png')} alt="icon" className="cardIcn"/>
            <div className="card-body">
            <p className="crd-title">$18,000</p><br/>
            <p className="card-text small">total-Revenue</p>
            </div>
            </div>

            <div className="card card-style">
            <img src={require('./Images/grp.png')} alt="icon" className="cardIcn"/>
            <div className="card-body">
            <p className="crd-title">1,000</p><br/>
            <p className="card-text2 small">total users</p>
            </div>
            </div>

            <div className="card card-style">
            <img src={require('./Images/strategy.png')} alt="icon" className="cardIcn"/>
            <div className="card-body">
            <p className="crd-title">250</p><br/>
            <p className="card-text3 small">total strategy</p>
            </div>
            </div>

            <div className="card card-style1">
            <img src={require('./Images/doller.png')} alt="icon" className="cardIcn"/>
            <div className="card-body">
            <p className="crd-title1">$1,000</p><br/>
            <p className="card-text4 small">Last Week Revenue</p>
            </div>
            </div>
            </div>
            <div className="chart">
            <Chart />
            </div>

            </div>
        </div>
        </div>   
        
    )
}
export default Dashboard;