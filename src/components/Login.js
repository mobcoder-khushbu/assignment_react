import React, { useReducer } from 'react';
import './signup.css';
import {Link,useHistory} from 'react-router-dom';
import { isValidEmail } from '../helpers';
import { loginAction } from '../actions';
import {connect} from 'react-redux';


const Login = props => {
    let history = useHistory();
    const [userInput, setUserInput] = useReducer((state, newState) => ({...state, ...newState}),
        {
            email: '',
            password: '',
        }
    );

    const [error, setError] = useReducer((state, newState) => ({...state, ...newState}),
        {
            email: '',
            password: ''
        }
    );



    const handleChange = e => {
        setUserInput({[e.target.name]: e.target.value});
        setError({[e.target.name]: ''});
    }

/*Validating the input Field */

    const handleValidate = () => {
        let validate = true;
        let {email, password } = userInput;
        if(email === ''){
            setError({email: 'email is require'});
            validate = false
        }
        else if(!isValidEmail(email)){
            setError({email: 'email is not valid'});
            validate = false
        }
        if(password === ''){
            setError({password: 'password is require'});
            validate = false
        }

        return validate;
    }

/*on form submit if input field validate then login */
    const handleSubmit = e => {
        e.preventDefault();
        if(handleValidate()){
            let {email, password } = userInput;
            let params = {
                email,
                password
            }

          /*Login Action perform */  
            props.loginAction(params)
            
            console.log('params',params);
            history.push("/dashboard");
        }
    }
return(
        <div> 
        <div className="container-fluid">
        <img src={require('./Images/background.jpg')} className="background" alt="food"/>
        <div className="carousel-caption">
        <ul className="nav">
        <li className="nav-item">
        <img src={require('./Images/Group 81.png')} alt="icon" className="logo"/>
        <span className="logo-text">GURU</span>
        </li>
        </ul>
        
        
        <div className="row form">
        <div className="col-sm-2"></div>
        <div className="col-sm-6">
        <form onSubmit={handleSubmit}>
        <img src={require('./Images/Group 81.png')} alt="icon"/><br/><br/>
        <span className="login-form-title">Sign in</span>
        <p>Sign in and start managing your business</p>
        <div className="Form">

        <div className="form-group">
		<input className="form-control input-form" type="text" name="email" placeholder="Email" 
        value={userInput.email} onChange={handleChange} />
        <div className="error">{error.email}</div>
        </div>

        <div className="form-group">
		<input className="form-control input-form" type="password" name="password" placeholder="Password" 
        value={userInput.password} onChange={handleChange} />
        <div className="error">{error.password}</div>
        </div>
       
        
        <div>
		<button  className="btn btn-primary btn" type="submit">
			Login
		</button>
		</div>
        <div><br/>
        <p><Link to='/forgot-password'>Forgot Password</Link></p>
        <span>Don't have an Account?<Link to='/signup'> Signup</Link></span>
        
        </div>
        </div>
        <div className="col-sm-3"></div>
        {props.errorMsg ? <h3>{props.errorMsg}</h3> : ''}
        </form>
        </div>
        </div>
        
         </div>
        </div>
         </div>
         
         
        );
    }

const mapStateToProps = state => {
    console.log(state, 'state')
    let { errorMsg } = state.auth; 
    return {
        errorMsg
    };
}
  
const mapDispatchToProps = dispatch => ({
    loginAction: params => dispatch(loginAction(params)),
});
  
export default connect(mapStateToProps, mapDispatchToProps)(Login);