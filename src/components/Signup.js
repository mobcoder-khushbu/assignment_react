import React, { useReducer } from 'react';
import './signup.css';
import {Link,useHistory} from 'react-router-dom';
import { isValidEmail } from '../helpers';
import { signupAction } from '../actions';
import {connect} from 'react-redux';

const Signup = props => {
let history = useHistory();
    const [userInput, setUserInput] = useReducer((state, newState) => ({...state, ...newState}),
        {
            name:'',
            email: '',
            password: '',
        }
    );

    const [error, setError] = useReducer((state, newState) => ({...state, ...newState}),
        {
            name: '',
            email: '',
            password: ''
        }
    );



    const handleChange = e => {
        setUserInput({[e.target.name]: e.target.value});
        setError({[e.target.name]: ''});
    }

/*Validate the input Field */

    const handleValidate = () => {
        let validate = true;
        let { name, email, password } = userInput;

        if(name === ''){
            setError({name: 'firstName is require'});
            validate = false
        }

        if(email === ''){
            setError({email: 'email is require'});
            validate = false
        }
        else if(!isValidEmail(email)){
            setError({email: 'email is not valid'});
            validate = false
        }
        if(password === ''){
            setError({password: 'password is require'});
            validate = false
        }

        return validate;
    }

/*on submit form checking the validation before submit */
    const handleSubmit = e => {
        e.preventDefault();

        if(handleValidate()){
            let {name, email, password } = userInput;
            let params = {
                name,
                email,
                password
            }

            props.signupAction(params)
            console.log('params',params);
            history.push("/login");
        }
    }
return(
        <div> 
        <div className="container-fluid">
        <img src={require('./Images/background.jpg')} className="background" alt="food"/>
        <div className="carousel-caption">
        <ul className="nav">
        <li className="nav-item">
        <img src={require('./Images/Group 81.png')} alt="icon" className="logo"/>
        <span className="logo-text">GURU</span>
        </li>
        </ul>
        
        
        <div className="row form">
        <div className="col-sm-2"></div>
        <div className="col-sm-6">
        <form onSubmit={handleSubmit}>
        <img src={require('./Images/Group 81.png')} alt="icon"/><br/><br/>
        <span className="login-form-title">Sign up</span>
        <div className="Form">
        <div className="form-group">
		<input className="form-control input-form" type="text" name="name" placeholder="Name"
        value={userInput.name}
        onChange={handleChange} />
        <div className="error">{error.name}</div>
        </div>

        <div className="form-group">
		<input className="form-control input-form" type="text" name="email" placeholder="Email" 
        value={userInput.email} onChange={handleChange} />
        <div className="error">{error.email}</div>
        </div>

        <div className="form-group">
		<input className="form-control input-form" type="password" name="password" placeholder="Password" 
        value={userInput.password} onChange={handleChange} />
        <div className="error">{error.password}</div>
        </div>
       
        
        <div>
		<button className="btn btn-primary btn" type="submit">
			Signup
		</button>
		</div>
        <div>
        <span>already have an Account? <Link to='/'> Login</Link></span>
        
        </div>
        </div>
        <div className="col-sm-3"></div>
        {props.errorMsg ? <h3>{props.errorMsg}</h3> : ''}
        </form>
        </div>
        </div>
        
         </div>
        </div>
         </div>
         
         
        );
    }

const mapStateToProps = state => {
    console.log(state, 'state')
    let { errorMsg } = state.auth; 
    return {
        errorMsg
    };
}
  
const mapDispatchToProps = dispatch => ({
    signupAction: params => dispatch(signupAction(params)),
});
  
export default connect(mapStateToProps, mapDispatchToProps)(Signup);
