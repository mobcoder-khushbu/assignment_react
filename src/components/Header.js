import React from 'react';
import { Nav } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import { AiOutlineAppstore,AiOutlineGroup } from 'react-icons/ai';
import { GrGroup} from 'react-icons/gr';
import './header.css';
const Header = props => {

    return(
        <div className="container-fluid">
        <div className="row">
        <div className="nav">
        <Nav>
        <ul className="nav nav flex-column">
        <li className="nav-item">
        <img src={require('./Images/Group 81.png')} alt="icon" className="logoIcon"/>
        </li>
        <li className="nav-item active">
        <Link className="nav-link n-t px" to="/dashboard"><AiOutlineAppstore className="react-icon"/></Link>
        </li>
        <li className="nav-item">
        <Link className="nav-link px-4" to="/userList"><GrGroup className="react-icon"/></Link>
        </li>
        <li className="nav-item">
        <Link className="nav-link" to="/dashboard"><AiOutlineGroup className="react-icon"/></Link>
        </li>
        </ul>
        </Nav>
        </div>
        </div>

        </div>



    )
}
export default Header;