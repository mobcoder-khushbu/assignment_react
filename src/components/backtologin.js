import React from 'react';
import './signup.css';
import {Link} from 'react-router-dom';



const BackLogin = props => {

    
return(
        <div> 
        <div className="container-fluid">
        <img src={require('./Images/background.jpg')} className="background" alt="food"/>
        <div className="carousel-caption">
        <ul className="nav">
        <li className="nav-item">
        <img src={require('./Images/Group 81.png')} alt="icon" className="logo"/>
        <span className="logo-text">GURU</span>
        </li>
        </ul>
         <div className="row form">
        <div className="col-sm-2"></div>
        <div className="col-sm-6">
        <form>
        <img src={require('./Images/Group 81.png')} alt="icon"/><br/><br/>
        <span className="login-form-title">Sign in</span>
        <p>Sign in and start managing your business</p>
        <div className="Form">
        <div>
		<Link className="btn btn-primary btn" type="submit" to='/'>
			Back To Login
		</Link>
		</div>
        </div>
        <div className="col-sm-3"></div>
        {props.errorMsg ? <h3>{props.errorMsg}</h3> : ''}
        </form>
        </div>
        </div>
        
         </div>
        </div>
         </div>
         
         
        );
    }


export default BackLogin;