import React, { useReducer } from 'react';
import './signup.css';
import {Link,useHistory} from 'react-router-dom';
import { isValidEmail } from '../helpers';
import { forgotAction } from '../actions';
import {connect} from 'react-redux';


const Forgot = props => {
    
    let history = useHistory();


    const [userInput, setUserInput] = useReducer((state, newState) => ({...state, ...newState}),
        {
            email: ''
        }
    );

    const [error, setError] = useReducer((state, newState) => ({...state, ...newState}),
        {
            email: ''
        }
    );



    const handleChange = e => {
        setUserInput({[e.target.name]: e.target.value});
        setError({[e.target.name]: ''});
    }



    const handleValidate = () => {
        let validate = true;
        let {email} = userInput;
        if(email === ''){
            setError({email: 'email is require'});
            validate = false
        }
        else if(!isValidEmail(email)){
            setError({email: 'email is not valid'});
            validate = false
        }
        return validate;
    }


    const handleSubmit = e => {
        e.preventDefault();

        if(handleValidate()){
            let {email } = userInput;
            let params = {
                email
                
            }

            props.forgotAction(params)
            console.log('params',params);
            history.push('/backtoLogin')
        }
    }
return(
        <div> 
        <div className="container-fluid">
        <img src={require('./Images/background.jpg')} className="background" alt="food"/>
        <div className="carousel-caption">
        <ul className="nav">
        <li className="nav-item">
        <img src={require('./Images/Group 81.png')} alt="icon" className="logo"/>
        <span className="logo-text">GURU</span>
        </li>
        </ul>
        
        
        <div className="row form">
        <div className="col-sm-2"></div>
        <div className="col-sm-6">
        <form onSubmit={handleSubmit}>
        <img src={require('./Images/Group 81.png')} alt="icon"/><br/><br/>
        <span className="login-form-title">Forgot Password</span>
        <p>Dont't worry enter the email associated with your account</p>
        <div className="Form">

        <div className="form-group">
		<input className="form-control input-form" type="text" name="email" placeholder="Email" 
        value={userInput.email} onChange={handleChange} />
        <div className="error">{error.email}</div>
        </div>
       
       <div>
		<button className="btn btn-primary btn" type="submit">
			Send
		</button>
		</div>
        <div><br/>
        <Link to='/'>Back to Login</Link>
        
        </div>
        </div>
        <div className="col-sm-3"></div>
        {props.errorMsg ? <h3>{props.errorMsg}</h3> : ''}
        </form>
        </div>
        </div>
        
         </div>
        </div>
         </div>
         
         
        );
    }

const mapStateToProps = state => {
    console.log(state, 'state')
    let { errorMsg } = state.auth; 
    return {
        errorMsg
    };
}
  
const mapDispatchToProps = dispatch => ({
    forgotAction: params => dispatch(forgotAction(params)),
});
  
export default connect(mapStateToProps, mapDispatchToProps)(Forgot);