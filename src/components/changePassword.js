import React, { useReducer } from 'react';
import './signup.css';
import Header from './Header';
import Navbar from './navbar';
import { resetAction } from '../actions';
import {connect} from 'react-redux';
import {useHistory} from 'react-router-dom';


const Change = props => {
    let history = useHistory();

    const [userInput, setUserInput] = useReducer((state, newState) => ({...state, ...newState}),
        {
            oldpassword: '',
            newpassword:'',
            cnfpassword:''
        }
    );

    const [error, setError] = useReducer((state, newState) => ({...state, ...newState}),
        {
            password: ''
            
        }
    );



    const handleChange = e => {
        setUserInput({[e.target.name]: e.target.value});
        setError({[e.target.name]: ''});
    }



    const handleValidate = () => {
        let validate = true;
        let {newpassword, cnfpassword} = userInput;
        if(newpassword === cnfpassword && newpassword===''){
            setError({password: 'password is require'});
            validate = false
        }
        else if(newpassword !== cnfpassword){
            setError({password:'password is not match'})
            validate=false
        }

        return validate;
    }


    const handleSubmit = e => {
        e.preventDefault();

        if(handleValidate()){
            let { newpassword,cnfpassword} = userInput;
            let params = {
                newpassword,
                cnfpassword
            }

            props.resetAction(params)
            console.log('params',params);
            history.push('/')
        }
    }

    return(
        <div className="container-fluid">
        <div className="row">
        <div className="nav">
            <Header/>
        </div>
        
         <div className="right-nav">
         <Navbar/>
         <div className="row">
           <div className="col-sm-6">
        <form onSubmit={handleSubmit}>

        <span className="login-form-title">Change Password</span>
        <div className="Form">

        <div className="form-group">
		<input className="form-control c-form" type="password" name="oldpassword" placeholder="Old Password" 
        value={userInput.oldpassword} onChange={handleChange} />
        <div className="error">{error.password}</div>
        </div>

        <div className="form-group">
		<input className="form-control c-form" type="password" name="newpassword" placeholder="New Password" 
        value={userInput.newpassword} onChange={handleChange} />
        <div className="error">{error.password}</div>
        </div>
        <div className="form-group">
		<input className="form-control c-form" type="password" name="cnfpassword" placeholder="Confirm Password" 
        value={userInput.cnfpassword} onChange={handleChange} />
        <div className="error">{error.password}</div>
        </div>
       
        
        <div>
		<button className="btn c-btn" type="submit" to='./'>
			Change Password
		</button>
		</div>
        <div><br/>
        
        </div>
        </div>
        <div className="col-sm-3"></div>
        {props.errorMsg ? <h3>{props.errorMsg}</h3> : ''}
        </form>
        </div>
            </div>
            

            </div>
        </div>
        </div>   
        
    )
}
const mapStateToProps = state => {
    console.log(state, 'state')
    let { errorMsg } = state.auth; 
    return {
        errorMsg
    };
}
  
const mapDispatchToProps = dispatch => ({
    resetAction: params => dispatch(resetAction(params)),
});
  
export default connect(mapStateToProps, mapDispatchToProps)(Change);