import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {Dropdown} from 'react-bootstrap';
import './sidebar.css';

export default class Navbar extends Component{


    render(){
        return(
            

<nav className="navbar navbar-white bg-white navbar-expand-sm navbr">
  <Link className="navbar-brand" to="#">
  <p className="text-dark">Khush</p>
  </Link>
 <Dropdown>
  <Dropdown.Toggle  id="dropdown-basic">
    <img src={require('./Images/profile.png')} alt="profile" width="40" height="40" className="rounded-circle"/>
  </Dropdown.Toggle>

  <Dropdown.Menu>
    <Link className="dropdown-item" to="/profile">Profile</Link>
    <Link className="dropdown-item" to="/changePassword">Change Password</Link>
    <Link className="dropdown-item" to="/">Logout</Link>
  </Dropdown.Menu>
</Dropdown>
</nav>
);
}
}