import React, {useEffect} from 'react';
import Header from './Header';
import Navbar from './navbar';
import profile from './Images/profile.png';
import './header.css';
import { getUser } from '../actions';
import {useSelector,useDispatch } from "react-redux";
import './header.css';

const ListUser = props => {

 const userData = useSelector(state => state.dataReducer)
 console.log('dhgvghd');
  const dispatch = useDispatch();

  const fetchData = () => {
        dispatch(getUser());
    }

    useEffect(fetchData,[])
  let imgsrc= (userData.profileImage && userData.profileImage.length !== 0) ? userData.profileImage : profile
    return(
        <div className="container-fluid">
        <div className="row">
        <div className="nav">
            <Header/>
        </div>
        
         <div className="right-nav">
         <Navbar />
         <div className="row">
            <div className="card list-card-style">
            <img src={require('./Images/grp.png')} alt="icon" className="cardIcn"/>
            <div className="card-body">
            <p className="crd-title">1,000</p><br/>
            <p className="card-text2 small">total users</p>
            </div>
            </div>
           
            <div className="card list-card-style">
            <img src={require('./Images/grp2.png')} alt="icon" className="cardIcn"/>
            <div className="card-body">
            <p className="crd-title">700</p><br/>
            <p className="card-text2 small">active users</p>
            </div>
            </div>
            
            <div className="card list-card-style">
            <img src={require('./Images/grp3.jpeg')} alt="icon" className="cardIcn"/>
            <div className="card-body">
            <p className="crd-title">300</p><br/>
            <p className="card-text2 small">inactive users</p>
            </div>
            </div>

            </div>
            <div>
              
              <table className="table">
              <thead>
                <tr>
                  <th scope="col">S.No.</th>
                  <th scope="col">Profile</th>
                  <th scope="col">Email</th>
                </tr>
              </thead>
              
              <tbody>
              {
                userData.length!==0 ? userData.map((user,index) =>
                <tr>
                  <td>{index + 1}</td>
                  <td><img className="profile" src={imgsrc} alt="profile"/></td>
                  <td>{user.email}</td>
                  
                </tr>
                ):null
                }
                </tbody>
              </table>
              
            </div>
              

            </div>
        </div>
        </div>   
        
    )
}


export default ListUser;
