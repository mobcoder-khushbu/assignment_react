import * as actionType from '../actions/actionType';

let auth = JSON.parse(localStorage.getItem('auth'));
const initialState = auth ? { isLogin: true, auth } : {};

/*Authantication Reducer perform here */
const authReducer = (state = initialState, action) => {

    switch (action.type) {


        case actionType.GET_AUTH_SUCCESS:
            return {
            isLogin: true,
            auth: action.payload
        }

        case actionType.GET_AUTH_FAILURE:
            return {
            isLogin: false,
            errorMsg: action.payload
        }
        case actionType.LOGOUT:
        return {};

        case actionType.GET_FORGOT_SUCCESS:
            return {
            isLogin: true,
            auth: action.payload
        }
        case actionType.GET_RESET_SUCCESS:
            return {
            isLogin: true,
            auth: action.payload
        }
        case actionType.GET_DATA:
            return {
            ...state,
            data: [action.payload]
        }


        default: return state;
    }
}
export default authReducer;