import {combineReducers} from 'redux';
import authReducer from './user.reducer';
import dataReducer from './getReducer';
const rootReducer = combineReducers({
  auth:authReducer,
  dataReducer
});

export default rootReducer;
