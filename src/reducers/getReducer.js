import * as actionType from '../actions/actionType';

/*Fetching data reducer perform here */

const user=[];

 const  dataReducer= (state=user,action)=>{
    switch(action.type){
        case actionType.GET_DATA:return [...action.payload]
        default: return state
    }
}

export default dataReducer;