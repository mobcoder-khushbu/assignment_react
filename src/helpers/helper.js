export const asyncCatch = fun => {
    return(req, res, next) => fun(req, res, next).catch(next);
}

export const resHandle = res => {
    let status = res.data.status;
    let data;
    if(status){
        console.log('<<<||| Success |||>>>', res)
        data = res.data.response
    }else{
        console.log('<<<||| Error ||||>>>', res)
        data = res.data.error.message
    }
    return { status, data }
}

export function authHeader() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user'));

    if (user && user.token) {
        return { 'Authorization': 'Bearer ' + user.token };
    } else {
        return {};
    }
}

const pattern = /(([a-zA-Z0-9\-?.?]+)@(([a-zA-Z0-9\-_]+\.)+)([a-z]{2,3}))+$/;
export const isValidEmail = e => new RegExp(pattern).test(e);