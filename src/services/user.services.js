import axios from 'axios';
import API from '../config/api';

const accessToken = localStorage.getItem('accessToken'); 
axios.defaults.headers.common['accessToken'] = `${accessToken}`;
axios.defaults.headers.common['Authorization'] = `${'Basic ZGVtb19hZG1pbjphZG1pbkBkZW1v'}`;
export const signup = params => {
        console.log("data in service1",API.USER_SIGNUP)

axios.post(API.USER_SIGNUP, params).then(res => {
    console.log("data in service",res)
    if(res.data.status){
        axios.defaults.headers.common['Authorization'] = `${'Basic ZGVtb19hZG1pbjphZG1pbkBkZW1v'}`;
    }
    return res;
});}

export const login = params => axios.post(API.USER_LOGIN, params).then(res => {
   
    localStorage.setItem('res', JSON.stringify(res.data.responseData.userProfile));

   
    return res;
});

export const logout = ()=>{
    // remove user from local storage to log user out
    localStorage.removeItem('res');
}



export const forgot = params => axios.post(API.USER_FORGOT, params).then(res => {
    if(res.data.status){
        axios.defaults.headers.common['accessToken'] = `${res.data.response.accessToken}`;
        axios.defaults.headers.common['Authorization'] = `${'Basic ZGVtb19hZG1pbjphZG1pbkBkZW1v'}`;
    }
    return res;
});
export const reset = (params,token) => axios.post(API.USER_RESET_PASSWORD, ({params,token})).then(res => {
    if(res.data.status){
        axios.defaults.headers.common['accessToken'] = `${token}`;
        axios.defaults.headers.common['Authorization'] = `${'Basic ZGVtb19hZG1pbjphZG1pbkBkZW1v'}`;
    }
    return res;
});

export function getData(){
    return axios.get(API.GET_DATA);
}

