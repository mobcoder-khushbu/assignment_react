import setting from './settings';
console.log(setting.api.url + "user/signUp");

export default  {
 
        'USER_SIGNUP': setting.api.url + "user/signUp",
        'USER_LOGIN': setting.api.url + "user/userLogin",
        'USER_FORGOT': setting.api.url + "user/forgotPassword",
        'USER_RESET_PASSWORD'  : setting.api.url + "user/resetPassword",
        'GET_DATA': setting.api.url + "user/getUsers?count=3&pageNo=1&status=1",
          }